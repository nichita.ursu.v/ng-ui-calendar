import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor () {

  }

  private getPreviousDaysLeft (daysLeft: number) {

    let result: string[] = [];

    const previousMonth: string = moment().subtract(1, 'M').format('MM-YYYY');
    const daysIn:        number = moment(previousMonth, 'MM-YYYY').daysInMonth();

    const monthLastDay:  string = moment(`${daysIn}-${previousMonth}`, 'DD-MM-YYYY').format();
    const leftDayStarts: number = +moment(monthLastDay).subtract(daysLeft - 2, 'd').format('DD');

    for (let i =  0; i < daysLeft - 1; i++) {

        console.log(`${ leftDayStarts + i }-${previousMonth}`);

        const newDate: string = moment( `${ leftDayStarts + i }-${previousMonth}`, 'DD-MM-YYYY' ).format();
        result.push(newDate);
    }

    return result;
  }

  private buildMonthInfo () {

    let currentMonth: string = moment().format('MM-YYYY');

    let daysIn:          number = moment().daysInMonth();
    let monthStartsFrom: number = moment(`01-${currentMonth}`, 'DD-MM-YYYY').weekday();

    let daysLeft: number = monthStartsFrom--;
    let daysLeftInfo: string[] = this.getPreviousDaysLeft(daysLeft);

    console.log('days left info', daysLeftInfo);
  }

  ngOnInit () {

    this.buildMonthInfo();
  }
}
